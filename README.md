# Table of Contents
***
1. [Technologies](#markdown-header-technologies)
2. [Installation](#markdown-header-installation)
3. [Future work](#markdown-header-future-work)
4. [Time taken](#markdown-header-time-taken)
***
# Technologies
### List of technologies used within the project:
- Symfony (version 4.4.3)
- PHP (version 7.4.14)
- MySQL (version 5.5.61)
- RabbitMQ (version N/A)
- Doctrine (version 2.7.1)
- List of additional Symfony Components installed:
	- symfony/http-client
	- symfony/serializer
	- symfony/property-info
	- symfony/property-access
	- php-amqplib/rabbitmq-bundle
	- symfony/orm-pack
	- symfony/serializer-pack
	- annotations
	- doctrine/doctrine-migrations-bundle
	- doctrine/orm
***
# Installation
### In order for the application to Run, the following steps must be performed:

1. `$git clone https://dimitrisor@bitbucket.org/dimitrisor/net2grid_php_assignment.git .`
2. Create the .env environment file and, 2.2 fill in the correct DATABASE_URL and RABBITMQ_URL environment variables
3. `$composer update`
4. `$php bin/console doctrine:migrations:migrate`
5. `$symfony server:start`
6. `$php bin/console rabbitmq:consumer messaging`
***
# Future work
### TODO things, that are missing from the current deliverable:

- Write unit and integration tests
- Add PHPDoc blocks for classes, methods, and functions
***
##### * Time taken
The implementation took approximately 3,5 full days of development including:
- 2 days for Learn the framework from 0 (how things work, research documentation, experiments)
- 1,5 days for Implementation