<?php /** @noinspection PhpIncompatibleReturnTypeInspection */

namespace App\Service\Result\Provider;

use App\Model\Dto\ResultDto;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ResultProviderService
{
    private HttpClientInterface $client;
    protected SerializerInterface $serializer;
    private LoggerInterface $logger;

    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->client = $httpClient;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function fetch(): ResultDto
    {
        $endpoint = 'https://a831bqiv1d.execute-api.eu-west-1.amazonaws.com/dev/results';
        $type = 'GET';
        $result = null;
        try {
            $response = $this->client->request($type, $endpoint);
            $data = $response->getContent();
            $result = $this->serializer->deserialize($data, ResultDto::class, 'json');
        } catch (HttpExceptionInterface $e) {
            $this->logger->error(sprintf("Fail to fetch Result in %s from url %s.", __CLASS__, $endpoint));
            throw new ResultServiceException(Response::HTTP_INTERNAL_SERVER_ERROR, "Something went wrong on our side, please try again in a few minutes", $e);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error(sprintf("Error at the transport level in result service %s.", $endpoint));
            throw new ResultServiceException(Response::HTTP_INTERNAL_SERVER_ERROR, "Something went wrong on our side, please try again in a few minutes", $e);
        }

        $this->logger->info(sprintf("Result (%s) fetched successfully to Queue from url %s.", $result->getRoutingKey(), $endpoint));

        return $result;
    }
}