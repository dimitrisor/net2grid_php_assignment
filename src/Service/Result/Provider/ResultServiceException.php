<?php

namespace App\Service\Result\Provider;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ResultServiceException extends HttpException
{
    public function __construct(int $statusCode, string $message = null, Exception $previous = null, array $headers = array(), $code = 0)
    {
        parent::__construct(
            $statusCode,
            $message,
            $previous,
            $headers,
            $code
        );
    }
}